{ config, pkgs, ... }:

{
  services.transmission = {
    enable = true;
    settings = {
      download-dir = "/var/lib/transmission/Downloads";
      rpc-enabled = true;
      rpc-host-whitelist-enabled = true;
      rpc-host-whitelist = "127.0.0.1";
    };
  };
}
