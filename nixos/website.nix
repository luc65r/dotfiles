{ config, pkgs, ... }:

{
  networking.firewall.enable = false;
  security.acme = {
    acceptTerms = true;
    certs = {
      "ransan.tk".email = "lucas@ransan.tk";
    };
  };
  services.nginx = {
    enable = true;
    virtualHosts."ransan.tk" = {
      forceSSL = true;
      enableACME = true;
      root = "/var/www/ransan.tk";
    };
  };
}
