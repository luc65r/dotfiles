{ config, pkgs, ... }:

{
  services.minecraft-server = {
    enable = true;
    eula = true;
    declarative = true;

    package = pkgs.minecraft-server.overrideAttrs (oldAttrs: rec {
      version = "20w13b";
      src = pkgs.fetchurl {
        url = "https://launcher.mojang.com/v1/objects/0e00cff8df2532a1ae252e773552c2fd6c68de80/server.jar";
        sha256 = "1njh58fyfsb77iws38pbw93k7bsgqi3lc4f558g1cy9r3lga7z36";
      };
    });

    serverProperties = {
      white-list = false;
      online-mode = false;
    };
  };
}
