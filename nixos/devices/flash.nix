{ config, pkgs, ... }:

{
  imports = [
    ../desktops/i3.nix
    ../binary-cache.nix
    ../website.nix
    ../stream.nix
    #../minecraft-forge.nix
    ../torrent.nix
    ../dlna.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    #plymouth.enable = true;
  };

  powerManagement.cpuFreqGovernor = "performance";
  hardware.cpu.amd.updateMicrocode = true;
  security.rngd.enable = false;
  nix = {
    nixPath = [
      "nixpkgs=/home/lucas/nixpkgs"
      "nixos-config=/etc/nixos/configuration.nix"
    ];
    maxJobs = 1;
    buildCores = 12;
    binaryCaches = [
      "https://meros.cachix.org"
    ];
    binaryCachePublicKeys = [
      "meros.cachix.org-1:Zp80aqT/HTZgS8FybS7UpDv8IPo2jsbCluiNQ1PbouQ="
    ];
  };

  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.enp4s0 = {
      ipv4.addresses = [
        { address = "192.168.0.10"; prefixLength = 24; }
      ];
      ipv6.addresses = [
        { address = "2a01:cb19:86ed:f600::10"; prefixLength = 56; }
      ];
    };
    defaultGateway = { address = "192.168.0.1"; interface = "enp4s0"; };
    defaultGateway6 = { address = "2a01:cb19:86ed:f600:46a6:1eff:fe80:c516"; interface = "enp4s0"; };
    nameservers = [ "84.200.69.80" "84.200.70.40" "2001:1608:10:25::1c04:b12f" "2001:1608:10:25::9249:d69b" ];
    hostName = "Flash";
  };


  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "be-latin1";
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # List packages installed in system profile.
  environment.systemPackages = import ../packages.nix pkgs;
  nixpkgs.config.allowUnfree = true;

  fonts = import ../fonts.nix pkgs;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  programs.fuse.userAllowOther = true;

  #programs.npm = {
  #  enable = true;
  #  npmrc = ''
  #    prefix = ''${HOME}/.npm
  #    color = true;
  #  '';
  #};

  programs.adb.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    knownHosts.titouan = {
      hostNames = [ "165.169.152.249" ];
      publicKeyFile = /home/lucas/.ssh/titouan.pub;
    };
    extraConfig = ''
      GatewayPorts yes
    '';
  };
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.system-config-printer.enable = true;
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      gutenprintBin
      cups-googlecloudprint
    ];
    browsing = true;
    defaultShared = true;
    extraConf = ''
      BrowseDNSSDSubTypes _cups,_print
      BrowseLocalProtocols all
      BrowseRemoteProtocols all
      CreateIPPPrinterQueues All
    '';
    browsedConf = ''
      BrowseDNSSDSubTypes _cups,_print
      BrowseLocalProtocols all
      BrowseRemoteProtocols all
      CreateIPPPrinterQueues All

      BrowseProtocols all
    '';
  };

  hardware.sane.enable = true;

  hardware.wooting.enable = true;

  services.avahi = {
    enable = true;
    nssmdns = true;
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    extraClientConf = "autospawn=yes\n";
    #extraConfig = ''
    #  load-module module-equalizer-sink
    #  load-module module-dbus-protocol
    #'';
  };

  # Fix a bug : no sound after suspend to RAM
  powerManagement.resumeCommands = ''
    echo 1 > '/sys/bus/pci/devices/0000:09:00.3/remove'
    echo 1 > '/sys/bus/pci/rescan'
  '';

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "be";
    #xkbOptions = "eurosign:e";

  # Enable proprietary NVIDIA drivers
    videoDrivers = [ "nvidia" ];

  # Enable touchpad support.
    libinput = {
      enable = true;
      accelProfile = "flat";
    };
  };

  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.lucas = {
    isNormalUser = true;
    home = "/home/lucas";
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "adbusers" "transmission" ]; # Enable ‘sudo’ for the user.
  };
  
  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  documentation.enable = false;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.09"; # Did you read the comment?

}
