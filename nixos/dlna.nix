{ config, pkgs, ... }:

{
  services.minidlna = {
    enable = true;
    mediaDirs = [
      "/home/lucas/Musique"
    ];
    announceInterval = 60;
  };
}
