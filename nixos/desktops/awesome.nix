{ pkgs, ... }:
{
  # Enable the Awesomme Window Manager
  services.xserver.windowManager.awesome.enable = true;
  services.xserver.displayManager.startx.enable = true;
  services.xserver.desktopManager.default = "none";
  services.xserver.windowManager.default = "awesome";

  environment.systemPackages = with pkgs; [
    st
    zathura
    #compton
    roboto
    #rofi
  ];
}
