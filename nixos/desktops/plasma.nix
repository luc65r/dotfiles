{ pkgs, ... }:
{
  # Enable the KDE Desktop Environment.
  services.xserver.displayManager = {
    sddm.enable = true;
    defaultSession = "plasma5";
  };
  services.xserver.desktopManager.plasma5.enable = true;

  environment.systemPackages = with pkgs; [
    okular
  ];
}
