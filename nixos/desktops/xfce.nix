{ pkgs, ... }:
{
  # Enable the Xfce Desktop Environment.
  services.xserver.desktopManager.xfce4-14.enable = true;
  services.xserver.desktopManager.default = "xfce4-14";

  services.xserver.displayManager.lightdm = {
    enable = true;
    #greeters.gtk = {
    #  enable = true;
    #  theme.name = "Chicago95";
    #};
  };

  environment.systemPackages = with pkgs; [
    xfce4-14.orage
  ];
}
