{ config, pkgs, ... }:

{
  networking.firewall.enable = false;
  security.acme = {
    acceptTerms = true;
    certs = {
      "binarycache.ransan.tk".email = "lucas@ransan.tk";
    };
  };
  services.nginx = {
    enable = true;
    virtualHosts."binarycache.ransan.tk" = {
      forceSSL = true;
      enableACME = true;
      locations."/".extraConfig = ''
        proxy_pass http://localhost:${toString config.services.nix-serve.port};
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      '';
    };
  };
  services.nix-serve = {
    enable = true;
    secretKeyFile = "/var/cache-priv-key.pem";
  };
}
