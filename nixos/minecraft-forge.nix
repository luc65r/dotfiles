{ lib, config, pkgs, ... }:

{
  services.minecraft-server = rec {
    enable = true;
    eula = true;
    declarative = true;
    dataDir = "/var/lib/minecraft-forge";

    package = pkgs.minecraft-server.overrideAttrs (oldAttrs: rec {
      pname = "minecraft-forge-server";
      version = "1.15.2-31.1.0";

      src = pkgs.fetchurl {
        url = "https://files.minecraftforge.net/maven/net/minecraftforge/forge/${version}/forge-${version}-installer.jar";
        sha256 = "19jm33igys0kgc5hiv1bj81nvizh4msz1x4yx1fgm1c1ycw1lwxb";
      };

      installPhase = /* sh */ ''
        mkdir -p $out/bin

        cat > $out/bin/minecraft-server << EOF
        #!/bin/sh
        exec ${pkgs.jre_headless}/bin/java \$@ -jar ./forge-${version}.jar nogui
        EOF

        chmod +x $out/bin/minecraft-server
      '';
    });

    serverProperties = {
      white-list = false;
      online-mode = false;
      difficulty = 3;
      gamemode = 0;
      force-gamemode = true;

    };
  };
}
