mountUSB: /* xml */ ''
<?xml version="1.0" encoding="UTF-8"?>

<openbox_menu xmlns="http://openbox.org/3.4/menu">

	<menu id="root-menu" label="Openbox 3">
		<item label="Monter">
			<action name="Execute">
				<command>sudo ${mountUSB}/bin/mountUSB</command>
				<startupnotify>
					<enabled>yes</enabled>
				</startupnotify>
			</action>
		</item>
		<item label="VLC">
			<action name="Execute">
				<command>vlc</command>
				<startupnotify>
					<enabled>yes</enabled>
				</startupnotify>
			</action>
		</item>
		<separator />
		<item label="Éteindre">
			<action name="Execute">
				<prompt>Éteindre ?</prompt>
				<execute>poweroff</execute>
			</action>
		</item>
	</menu>

</openbox_menu>
''
