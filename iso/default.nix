{ nixpkgs ? <nixpkgs>
, system ? "x86_64-linux"
}:

let
  mountUSB = pkgs: pkgs.writeScriptBin "mountUSB" /* sh */ ''
    #!${pkgs.bash}/bin/bash
    set +e
    for usb in /dev/sd?? ; do
      mkdir -p /home/pat$usb
      mount $usb /home/pat$usb || echo "cannot mount $usb"
    done
  '';

  isoConfig = { pkgs, lib, ... }: {
    imports = [
      "${nixpkgs}/nixos/modules/installer/cd-dvd/iso-image.nix"
    ];

    boot = {
      kernelPackages = pkgs.linuxPackages_latest;
      kernel.sysctl = {
        "vm.dirty_background_ratio" = 50;
        "vm.dirty_ratio" = 80;
      };
      loader.timeout = lib.mkForce 0;
    };

    isoImage = {
      isoName = "VLC.iso";
      makeEfiBootable = false;
      makeUsbBootable = true;
    };

    networking = {
      dhcpcd.enable = false;
      firewall.enable = false;
    };

    services.xserver = {
      enable = true;
      videoDrivers = [
        "intel"
        "nouveau"
        "ati"
        "amdgpu"
        "radeon"
        "cirrius"
        "fbdev"
        "vesa"
        "modesetting"
      ];
      displayManager = {
        defaultSession = "none+openbox";
        lightdm = {
          enable = true;
          autoLogin = {
            enable = true;
            user = "pat";
          };
        };
      };
      windowManager.openbox.enable = true;
    };

    environment.etc = {
      "xdg/openbox/menu.xml".text = import ./menu.nix (mountUSB pkgs);
      "xdg/openbox/rc.xml".source = ./rc.xml;
    };

    users = {
      mutableUsers = false;
      users.pat = {
        isNormalUser = true;
        extraGroups = [ "wheel" "video" ];
        password = "";
      };
      users.root.password = "";
    };
    services.mingetty.autologinUser = "pat";

    security.sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };

    security.polkit.extraConfig = ''
      polkit.addRule(function(action, subject) {
        if (subject.isInGroup("wheel")) {
          return polkit.Result.YES;
        }
      });
    '';

    hardware.pulseaudio.enable = true;

    environment.systemPackages = with pkgs; [
      vlc
    ];

    system.stateVersion = "20.09";
  };

  evalNixOS = configuration: import "${nixpkgs}/nixos" {
    inherit system configuration;
  };

in {
  iso = (evalNixOS isoConfig).config.system.build.isoImage;
}
