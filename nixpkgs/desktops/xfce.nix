{ ... }:
{
  home.file = {
    ".themes/Chicago95/".source = ./xfce/Theme/Chicago95;
    ".icons/Chicago95/".source = ./xfce/Icons/Chicago95;
    ".icons/Chicago95_Cursor_White/".source = ./xfce/Icons/Chicago95_Cursor_White;
    ".icons/default/".source = ./xfce/Icons/default;
    ".gtkrc-2.0".source = ./xfce/gtkrc-2.0;
    ".config/gtk-3.0/settings.ini".source = ./xfce/gtkrc-3.0;
    ".config/xfce4/xfconf/xfce-perchannel-xml/".source = ./xfce/xml;
    ".config/xfce4/wallpaper95.jpg".source = ./xfce/wallpaper95.jpg;
  };
}
