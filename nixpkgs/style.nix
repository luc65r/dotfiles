rec {
  font = {
    mono = "Iosevka Fixed";
    sans = "Iosevka Sparkle";
    serif = "Iosevka Etoile";
    icons = "Material Design Icons";
    size = 12;
  };
  color = import ./colorscemes/dracula.nix;
}
