{ pkgs, ... }:

let
  tlauncher = pkgs.callPackage ./pkgs/tlauncher.nix {};
  fvi = pkgs.callPackage ./pkgs/fvi.nix {};
  fcd = pkgs.callPackage ./pkgs/fcd.nix {};
  kp = pkgs.callPackage ./pkgs/kp.nix {};
  deemix = pkgs.python3Packages.callPackage ./pkgs/deemix.nix {};

in with pkgs; [
  # Hardware
  lm_sensors
  stress
  neofetch
  iftop

  # Image
  graphviz
  sxiv

  # Video
  vlc
  ffmpeg

  # Audio
  mpc_cli
  ncmpcpp
  audacity
  lilypond
  timidity
  musescore
  deemix

  # Chat
  discord
  riot-desktop
  tdesktop
  zoom-us

  # Code
  #shellcheck
  arduino
  python3
  love_11
  gcc
  ghc
  gnumake

  # Rust
  rustc
  cargo

  # Useless
  cmatrix
  lolcat

  # Text
  texlive.combined.scheme-full
  libreoffice
  pandoc
  translate-shell

  # Android
  mtpfs

  # Game
  glxinfo
  pcsx2
  steam
  (steam.override {
    nativeOnly = true;
  }).run
  minetest
  stepmania
  tlauncher

  # Compress
  zip unzip
  p7zip

  # Emulation
  qemu

  # 3D
  blender

  # Browser
  w3m
  ungoogled-chromium

  # Files
  ranger
  nnn
  tree
  fd
  ripgrep
  jq
  fzf
  fvi
  fcd
  kp

  # Disk
  gptfdisk
  parted

  # nixpkgs
  nix-prefetch-github
  nix-prefetch-git

  # X
  xdotool
  keynav

  # Internet
  bind

  # Dactylographie
  gtypist
  ktouch

  # Help
  tldr
]
