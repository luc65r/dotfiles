{ stdenvNoCC
, writeScriptBin
, fd
, fzf
}:

writeScriptBin "fvi" /* sh */ ''
  #!${stdenvNoCC.shell}
  file=$(${fd}/bin/fd -HIt f -E .git -E .cargo -E .cache . | ${fzf}/bin/fzf)
  [ "$?" -eq "0" ] && [ -n "$EDITOR" ] && $EDITOR "$file"
''
