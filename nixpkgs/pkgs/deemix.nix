{ lib
, buildPythonPackage
, fetchgit
, click
, pycryptodomex
, mutagen
, requests
, spotipy
}:

buildPythonPackage rec {
  pname = "deemix";
  version = "2020-05-30";

  src = fetchgit {
    url = "https://notabug.org/RemixDev/deemix";
    rev = "c0dee07010eb8e6cf14e7a169fa53fa0814741ef";
    sha256 = "15azsklnvg1vc8pkhwgk2m0fmk4xdal2qibibsvxg3yzz8kzrqql";
  };

  propagatedBuildInputs = [
    click
    pycryptodomex
    mutagen
    requests
    spotipy
  ];

  doCheck = false; # FIXME: Make tests work

  meta = with lib; {
    description = "A deezer downloader";
    homepage = "https://deemix.app/";
    license = licenses.gpl3;
    maintainers = with maintainers; [ luc65r ];
  };
}
