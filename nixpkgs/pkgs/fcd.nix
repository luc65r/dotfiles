{ stdenvNoCC
, writeScriptBin
, fd
, fzf
}:

writeScriptBin "fcd" /* sh */ ''
  #!${stdenvNoCC.shell}
  cd "$HOME"
  dir=$(${fd}/bin/fd -HIt d -E .git -E .cargo . | ${fzf}/bin/fzf)
  [ "$?" -eq "0" ] && cd "$dir"
''
