{ buildFHSUserEnv
, fetchurl
, jre
, xorg
}:

let
  pname = "tlauncher";
  version = "1.114.4";

  tlauncher = fetchurl {
    name = "${pname}-${version}.jar";
    url = "https://tlaun.ch/download.php?&package=jar";
    sha256 = "1w28m1ppbrsr6gc5hms71xlvxjawwqyk3f0kd88z4c6f2h08vah0";
  };

in buildFHSUserEnv {
  name = pname;

  targetPkgs = pkgs: [
    jre
    xorg.libXxf86vm
  ];
  
  runScript = /* sh */ ''
    java -d64 -jar ${tlauncher}
  '';
}
