{ stdenvNoCC
, writeScriptBin
, fzf
}:

writeScriptBin "kp" /* sh */ ''
  #!${stdenvNoCC.shell}
  for p in "$(ps -A | awk '{$2=$3=""; print $0}' | fzf -m)"; do
    kill -9 "$(echo $p | awk '{print $1}')"
  done
''
