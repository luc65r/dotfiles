{ ... }:

let style = import ../style.nix;

in {
  programs.zathura = {
    enable = true;
    options = with style; {
      recolor = true;
      font = font.mono + " " + toString font.size;
      default-fg = color.foreground;
      default-bg = color.background;
      recolor-darkcolor = color.foreground;
      recolor-lightcolor = color.background;
    };
  };
}
