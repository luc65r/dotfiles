{ pkgs, ... }:

let style = import ../style.nix;

in {
  programs.rofi = with style; {
    enable = true;
    width = null;
    lines = null;
    borderWidth = 0;
    rowHeight = 1;
    padding = null;
    font = font.mono + " " + toString font.size;
    scrollbar = false;
    terminal = "alacritty";
    location = "center";
    colors = with color; {
      window = {
        inherit background;
        border = background;
        separator = selection;
      };
      rows = {
        normal = {
          inherit background foreground;
          backgroundAlt = background;
          highlight = {
            background = selection;
            inherit foreground;
          };
        };
      };
    };
    package = pkgs.rofi.override {
      plugins = with pkgs; [
        rofi-calc
      ];
    };
  };
}
