{ ... }:

let style = import ../style.nix;

in {
  programs.alacritty = {
    enable = true;
    settings = {
      cursor.style = "Beam";
      window.padding = { x = 5; y = 5; };
      colors = with style.color; {
        primary = {
          inherit background foreground;
        };
        cursor = {
          text = selection;
          cursor = foreground;
        };
        normal = {
          inherit black red green yellow blue magenta cyan white;
        };
        inherit bright dim;
      };
      font = with style.font; {
        size = size;
        normal = {
          family = mono;
          style = "Regular";
        };
        bold = {
          family = mono;
          style = "Bold";
        };
        italic = {
          family = mono;
          style = "Italic";
        };
        bold_italic = {
          family = mono;
          style = "Bold Italic";
        };
      };
    };
  };
}
