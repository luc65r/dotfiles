{ ... }:

let style = import ../style.nix;

in {
  programs.kitty = with style; {
    enable = true;
    settings = with color; {
      font_family = font.mono;
      font_size = font.size;
      disable_ligatures = "never";
      font_features = "FiraCode +ss06 +ss07";

      cursor_shape = "beam";

      enable_audio_bell = false;

      window_padding_width = 2;

      background_opacity = "0.9";

      inherit foreground background;
      selection_foreground = selection;
      selection_background = foreground;
      color0 = black;
      color1 = red;
      color2 = green;
      color3 = yellow;
      color4 = blue;
      color5 = magenta;
      color6 = cyan;
      color7 = white;
      color8 = bright.black;
      color9 = bright.red;
      color10 = bright.green;
      color11 = bright.yellow;
      color12 = bright.blue;
      color13 = bright.magenta;
      color14 = bright.cyan;
      color15 = bright.white;
    };
  };
}
