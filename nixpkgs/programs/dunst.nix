{ ... }:

let style = import ../style.nix;

in {
  services.dunst = {
    enable = true;
    #iconTheme = pkgs.hicolor-icon-theme;
    settings = {
      global = with style; {
        geometry = "300x5-30+50";
        font = font.mono + " " + toString font.size;
        frame_color = color.background;
      };
      urgency_low = with style.color; {
        inherit background foreground;
        timeout = 5;
      };
      urgency_normal = with style.color; {
        inherit background foreground;
        timeout = 10;
      };
      urgency_critical = with style.color; {
        inherit background;
        foreground = red;
        timeout = 5;
      };
    };
  };
}
