# My NixOS and Home Manager configuraion

## Instalation

The nixos directory goes in /etc/
```console
$ sudo ln -s /path/to/repo/nixos/ /etc/
```

The nixpkgs directory goes in ~/.config/
```console
$ ln -s /path/to/repo/nixpkgs/ /home/user/.config/
```

## Devices

### Flash

* Usage: 2018-
* CPU: Ryzen 7 1700 @ 4 Ghz
* RAM: 16 GB
* Motherboard: Gigabyte Aorus AX370-Gaming K5
* Graphic card: Gigabyte Aorus GTX 1080
* Water cooling: Cooler Master MasterLiquid 240
* Power supply: Corsair HX850
* Case: Antec P280
* SSD: Samsung 256 GB
* HDD: 2 TB, 7200 rpm
* Monitor: Samsung C24FG73FQU (24', 1080p, 144 Hz)
* Mouse: Logitech G402
* Keyboard: Wooting One with Flaretech Clicky55
